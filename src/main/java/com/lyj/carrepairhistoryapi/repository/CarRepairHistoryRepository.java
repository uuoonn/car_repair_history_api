package com.lyj.carrepairhistoryapi.repository;


import com.lyj.carrepairhistoryapi.entity.CarRepairHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepairHistoryRepository extends JpaRepository<CarRepairHistory,Long> {
}
