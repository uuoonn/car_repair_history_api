package com.lyj.carrepairhistoryapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class RepairCostChangeRequest {
    private Double repairCost;
}
