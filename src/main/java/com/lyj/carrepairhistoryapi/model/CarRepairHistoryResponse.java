package com.lyj.carrepairhistoryapi.model;

import com.lyj.carrepairhistoryapi.enums.BrokenPart;
import com.lyj.carrepairhistoryapi.enums.CarName;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class CarRepairHistoryResponse {
    private Long id;
    private String carNameName;
    private Double carNamePrice;
    private LocalDate problemDate;
    private String brokenPartName;
    private String repairMemo;
    private String isSelf;
    private String repairShop;
    private LocalDate repairDate;
    private Double repairCost;
    private String isCompleted;
}
