package com.lyj.carrepairhistoryapi.model;

import com.lyj.carrepairhistoryapi.enums.CarName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class RepairHistoryItem {
    private Long id;
    private String carNameName;
    private String isSelfName;
    private String isCompletedName;


}
