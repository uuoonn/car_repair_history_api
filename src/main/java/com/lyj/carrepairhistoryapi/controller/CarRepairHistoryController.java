package com.lyj.carrepairhistoryapi.controller;

import com.lyj.carrepairhistoryapi.entity.CarRepairHistory;
import com.lyj.carrepairhistoryapi.model.CarRepairHistoryRequest;
import com.lyj.carrepairhistoryapi.model.CarRepairHistoryResponse;
import com.lyj.carrepairhistoryapi.model.RepairCostChangeRequest;
import com.lyj.carrepairhistoryapi.model.RepairHistoryItem;
import com.lyj.carrepairhistoryapi.service.CarRepairHistoryService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/carinfo")

public class CarRepairHistoryController {
    private final CarRepairHistoryService carRepairHistoryService;

    @PostMapping("/new")
    public String setCarRepairHistory(@RequestBody CarRepairHistoryRequest request){
        carRepairHistoryService.setCarRepairHistory(request);

        return "ok";
    }

    @GetMapping("/histories")
    public List<RepairHistoryItem> getRepairHistories(){
        return carRepairHistoryService.getRepairHistories();
    }

    @GetMapping("/detail/{id}")
    public CarRepairHistoryResponse getRepairHistory(@PathVariable long id){
        return carRepairHistoryService.getRepairHistory(id);
    }


    @PutMapping("/cost/{id}")
    public String putCarRepairHistory(@PathVariable long id, @RequestBody RepairCostChangeRequest request){
        carRepairHistoryService.putCarRepairHistory(id, request);

        return "ok";
    }
}
