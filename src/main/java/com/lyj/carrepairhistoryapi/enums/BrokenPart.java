package com.lyj.carrepairhistoryapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor

public enum BrokenPart {
    FRONT_PANEL("프론트 판넬"),
    BONNET("본네트"),
    LOOF_PANEL("루프판넬"),
    TRUNK("트렁크"),
    REAR_PANEL("리어판넬"),
    FRONT_WHEELHOUSE("앞휠하우스"),
    FILLERS("필러"),
    FRONT_DOOR("앞문"),
    BACK_DOOR("뒷문"),
    BACK_FENDER("뒤펜더"),
    REAR_WHEELHOUSE("뒤휠하우스");

    private final String name;





}
