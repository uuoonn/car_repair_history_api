package com.lyj.carrepairhistoryapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor

public enum CarName {
    AVANTE("아반떼",20000000D),
    FORTE("포르테",30000000D),
    SONATA("소나타",40000000D),
    K3("K3",20000000D),
    K5("K5",50000000D),
    K7("K7",60000000D),
    GRANGER("그랜저",40000000D),
    GENESIS("제네시스",30000000D);

    private final String name;
    private final Double carPrice;

}
