package com.lyj.carrepairhistoryapi.entity;

import com.lyj.carrepairhistoryapi.enums.BrokenPart;
import com.lyj.carrepairhistoryapi.enums.CarName;
import jakarta.persistence.*;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter

public class CarRepairHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private CarName carName;

    @Column(nullable = false)
    private LocalDate problemDate;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private BrokenPart brokenPart;

    @Column(length = 40)
    private String repairMemo;

    @Column(length = 15)
    private Boolean isSelf;

    @Column(length = 15)
    private String repairShop;

    private LocalDate repairDate;

    private Double repairCost;

    @Column(nullable = false)
    private Boolean isCompleted;


}
