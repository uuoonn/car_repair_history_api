package com.lyj.carrepairhistoryapi.service;

import com.lyj.carrepairhistoryapi.entity.CarRepairHistory;
import com.lyj.carrepairhistoryapi.enums.CarName;
import com.lyj.carrepairhistoryapi.model.CarRepairHistoryRequest;
import com.lyj.carrepairhistoryapi.model.CarRepairHistoryResponse;
import com.lyj.carrepairhistoryapi.model.RepairCostChangeRequest;
import com.lyj.carrepairhistoryapi.model.RepairHistoryItem;
import com.lyj.carrepairhistoryapi.repository.CarRepairHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CarRepairHistoryService {
    private final CarRepairHistoryRepository carRepairHistoryRepository;

    public void setCarRepairHistory (CarRepairHistoryRequest request) {
        CarRepairHistory addData = new CarRepairHistory();
        addData.setCarName(request.getCarName());
        addData.setProblemDate(request.getProblemDate());
        addData.setBrokenPart(request.getBrokenPart());
        addData.setRepairMemo(request.getRepairMemo());
        addData.setIsSelf(request.getIsSelf());
        addData.setRepairShop(request.getRepairShop());
        addData.setRepairDate(request.getRepairDate());
        addData.setRepairCost(request.getRepairCost());
        addData.setIsCompleted(request.getIsCompleted());

        carRepairHistoryRepository.save(addData);


    }


    public List<RepairHistoryItem> getRepairHistories(){
        List<CarRepairHistory> originData = carRepairHistoryRepository.findAll();

        List<RepairHistoryItem> result = new LinkedList<>();

        for(CarRepairHistory carRepairHistory : originData){
            RepairHistoryItem addItem = new RepairHistoryItem();
            addItem.setId(carRepairHistory.getId());
            addItem.setCarNameName(carRepairHistory.getCarName().getName());
            addItem.setIsSelfName(carRepairHistory.getIsSelf() ? "자가수리완료" : "자가수리x");
            addItem.setIsCompletedName(carRepairHistory.getIsCompleted() ? "수리완료" : "수리예정");
            result.add(addItem);

        }
        return result;
    }

    public CarRepairHistoryResponse getRepairHistory(long id){
        CarRepairHistory originData = carRepairHistoryRepository.findById(id).orElseThrow();

        CarRepairHistoryResponse response = new CarRepairHistoryResponse();
        response.setId(originData.getId());
        response.setCarNameName(originData.getCarName().getName());
        response.setCarNamePrice(originData.getCarName().getCarPrice());
        response.setProblemDate(originData.getProblemDate());
        response.setBrokenPartName(originData.getBrokenPart().getName());
        response.setRepairMemo(originData.getRepairMemo());
        response.setIsSelf(originData.getIsSelf() ? "자가수리완료" : "자가수리x");
        response.setRepairShop(originData.getRepairShop());
        response.setRepairDate(originData.getRepairDate());
        response.setRepairCost(originData.getRepairCost());
        response.setIsCompleted(originData.getIsCompleted() ? "수리완료" : "수리예정");

        return response;


    }

    public void putCarRepairHistory (long id,RepairCostChangeRequest request){
        CarRepairHistory originData = carRepairHistoryRepository.findById(id).orElseThrow();
        originData.setRepairCost(request.getRepairCost());

        carRepairHistoryRepository.save(originData);

    }

}
